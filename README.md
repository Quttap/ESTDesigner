# ESTDesigner
-------------

> 关于我，欢迎关注  
  博客：http://lisonghua2010.iteye.com/

基于Draw2d Touch实现的Activiti工作流Web设计器，本设计器完全使用JQuery语法开发，集成Easyui前端框架，支持目前多种主流浏览器。

#### 示例:  
![流程绘制](https://images.gitee.com/uploads/images/2018/0912/100713_d2eb3ea6_449577.jpeg "绘制流程.JPG")
![生成XML](https://images.gitee.com/uploads/images/2018/0912/100742_5db185a9_449577.jpeg "生成文档.JPG")

### 特性

- 多浏览器支持
- 支持Activiti5语法结构
- 采用目前比较流行的JS库-JQuery
- 符合BPMN2.0标准
- 可视化定制
- 代码简洁易维护

### 依赖
- Draw2D V6.1.66
- Jquery V1.12.0
- EasyUI V1.4.5


### 安装部署

直接导入Eclipse，用HttpReview运行

访问地址
http://localhost:8080/estd/index.html


### 功能

- 支持拖拽式流程绘制
- 依赖流程图动态生成XML文档
- 根据XML文档生成流程图
- 支持子流程


### 打赏
![输入图片说明](https://images.gitee.com/uploads/images/2018/0912/120904_aa52b563_449577.png "微信图片_20180912120440.png")